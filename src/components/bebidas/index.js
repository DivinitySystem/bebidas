import axios from "axios";
import { useEffect, useState } from "react";
import { useContext } from "react";
import { MainList } from "../../styles/list";
import { DivApp } from "../../styles/app";
import { CasamentoContext } from "../../providers/wedding";
import { ConfraternizacaoContext } from "../../providers/confraternization";
import { FormaturaContext } from "../../providers/graduation";
const Home = () => {
  const [catalogue, setCatalogue] = useState([]);
  const [page, setPage] = useState(1);
  const { addToConfraternizationList } = useContext(ConfraternizacaoContext);
  const { addToWeddingList } = useContext(CasamentoContext);
  const { addToGraduationList } = useContext(FormaturaContext);

  const nextPage = () => {
    if (page <= 12) {
      setPage(page + 1);
    }
  };
  const previousPage = () => {
    if (page >= 2) {
      setPage(page - 1);
    }
  };
  useEffect(() => {
    axios
      .get(`https://api.punkapi.com/v2/beers?page=${page}`)
      .then((res) => setCatalogue(res.data));
  }, [page]);

  return (
    <DivApp>
      <div>
        <div>
          <button onClick={previousPage}>Pagina Anterior</button>
          <button onClick={nextPage}>Próxima página</button>
        </div>
        Página: {page}
      </div>
      <MainList>
        {catalogue.map((item) => (
          <li key={item.id}>
            {item.name}

            <img src={item.image_url} alt="404"></img>

            <div>
              <button onClick={() => addToGraduationList(item)}>
                Adicionar na formatura
              </button>
              <button onClick={() => addToWeddingList(item)}>
                Adicionar no Casamento
              </button>
              <button onClick={() => addToConfraternizationList(item)}>
                Adicionar na Confraternização
              </button>
            </div>
          </li>
        ))}
      </MainList>
    </DivApp>
  );
};

export default Home;
