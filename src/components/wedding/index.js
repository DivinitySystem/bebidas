import { useContext } from "react";
import { CasamentoContext } from "../../providers/wedding";
import { MainList } from "../../styles/list";

export const Weddings = () => {
  const { removeFromWeddingList, weddingList } = useContext(CasamentoContext);
  return (
    <MainList>
      {weddingList.map((item) => (
        <li key={item.id}>
          {item.name}

          <img src={item.image_url} alt="404"></img>

          <div>
            <button onClick={() => removeFromWeddingList(item)}>
              Remover do Casamento
            </button>
          </div>
        </li>
      ))}
    </MainList>
  );
};

export default Weddings;
