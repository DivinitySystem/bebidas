import { useContext } from "react";
import { ConfraternizacaoContext } from "../../providers/confraternization";
import { MainList } from "../../styles/list";

export const Confraternization = () => {
  const { removeFromConfraternizationList, confraternizationList } = useContext(
    ConfraternizacaoContext
  );
  return (
    <MainList>
      {confraternizationList.map((item) => (
        <li key={item.id}>
          {item.name}

          <img src={item.image_url} alt="404"></img>

          <div>
            <button onClick={() => removeFromConfraternizationList(item)}>
              Remover da Confraternizacao
            </button>
          </div>
        </li>
      ))}
    </MainList>
  );
};
export default Confraternization;
