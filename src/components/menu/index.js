import { Toolbar, AppBar, MenuItem } from "@material-ui/core";
import { useHistory } from "react-router";

const Menu = () => {
  const history = useHistory();
  const sendTo = (path) => {
    history.push(path);
  };
  return (
    <AppBar position="static">
      <Toolbar>
        <MenuItem onClick={() => sendTo("/")}>Bebidas</MenuItem>
        <MenuItem onClick={() => sendTo("/confraternizacao")}>
          Confraternizacão
        </MenuItem>
        <MenuItem onClick={() => sendTo("/casamento")}>Casamento</MenuItem>
        <MenuItem onClick={() => sendTo("/formatura")}>Formatura</MenuItem>
      </Toolbar>
    </AppBar>
  );
};
export default Menu;
