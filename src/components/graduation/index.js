import { useContext } from "react";
import { FormaturaContext } from "../../providers/graduation";
import { MainList } from "../../styles/list";

export const Graduation = () => {
  const { removeFromGraduationList, formaturaList } =
    useContext(FormaturaContext);
  return (
    <MainList>
      {formaturaList.map((item) => (
        <li key={item.id}>
          {item.name}

          <img src={item.image_url} alt="404"></img>

          <div>
            <button onClick={() => removeFromGraduationList(item)}>
              Remover da Formatura
            </button>
          </div>
        </li>
      ))}
    </MainList>
  );
};

export default Graduation;
