import styled from "styled-components";
export const MainList = styled.ul`
  display: flex;
  flex-direction: row;
  align-items: center;
  flex-wrap: wrap;
  list-style: none;
  padding: 0;
  li {
    align-items: center;
    justify-content: center;
    display: flex;
    flex-direction: column;
    margin: 35px;
    background-color: #a5facc;
    padding: 80px;
    width: 341.465px;
    border-radius: 10px;
    img {
      width: 100px;
      height: 300px;
      margin: 20px;
    }
    div {
      display: flex;
      flex-direction: row;
      margin-left: 20px;
    }
    ul {
      display: flex;
      flex-direction: column;
      height: 10px;
    }
    button {
      margin: 0;
      margin-left: 10px;
      background-color: #1e6d51;
      color: white;
      width: 140px;
      height: 40px;
      border-radius: 20px;
      cursor: pointer;
    }
    button:hover {
      opacity: 0.7;
    }
  }
`;
