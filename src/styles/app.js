import styled from "styled-components";

export const DivApp = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;

  button {
    margin-left: 10px;
    background-color: #1e6d51;
    color: white;
    width: 140px;
    height: 40px;
    border-radius: 20px;
    cursor: pointer;
    margin: 20px;
  }

  button:hover {
    opacity: 0.7;
  }
`;
