import { Switch, Route } from "react-router-dom";
import Home from "../components/bebidas";
import Confraternization from "../components/confraternization";
import Graduation from "../components/graduation";
import Weddings from "../components/wedding";
const Routes = () => {
  return (
    <Switch>
      <Route exact path="/formatura">
        <Graduation></Graduation>
      </Route>
      <Route exact path="/casamento">
        <Weddings></Weddings>
      </Route>
      <Route exact path="/confraternizacao">
        <Confraternization></Confraternization>
      </Route>
      <Route exact path="/">
        <Home></Home>
      </Route>
    </Switch>
  );
};
export default Routes;
