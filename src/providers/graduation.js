import { createContext, useState } from "react";
export const FormaturaContext = createContext([]);
export const FormaturaProvider = ({ children }) => {
  const [formaturaList, setFormaturaList] = useState([]);
  const addToGraduationList = (item) => {
    if (!formaturaList.find((name) => name.id === item.id)) {
      setFormaturaList([...formaturaList, item]);
    }
  };
  const removeFromGraduationList = (item) => {
    const newList = formaturaList.filter((name) => name.id !== item.id);
    setFormaturaList(newList);
  };
  return (
    <FormaturaContext.Provider
      value={{ removeFromGraduationList, addToGraduationList, formaturaList }}
    >
      {children}
    </FormaturaContext.Provider>
  );
};
