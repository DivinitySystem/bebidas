import { createContext, useState } from "react";
export const ConfraternizacaoContext = createContext([]);
export const ConfraternizacaoProvider = ({ children }) => {
  const [confraternizationList, setConfraternizationList] = useState([]);

  const addToConfraternizationList = (item) => {
    if (!confraternizationList.find((name) => name.id === item.id)) {
      setConfraternizationList([...confraternizationList, item]);
    }
  };
  const removeFromConfraternizationList = (item) => {
    const newList = confraternizationList.filter((name) => name.id !== item.id);
    setConfraternizationList(newList);
  };
  return (
    <ConfraternizacaoContext.Provider
      value={{
        removeFromConfraternizationList,
        addToConfraternizationList,
        confraternizationList,
      }}
    >
      {children}
    </ConfraternizacaoContext.Provider>
  );
};
