import { createContext, useState } from "react";
export const CasamentoContext = createContext([]);
export const CasamentoProvider = ({ children }) => {
  const [weddingList, setWeddingList] = useState([]);

  const addToWeddingList = (item) => {
    if (!weddingList.find((name) => name.id === item.id)) {
      setWeddingList([...weddingList, item]);
    }
  };

  const removeFromWeddingList = (item) => {
    const newList = weddingList.filter((name) => name.id !== item.id);
    setWeddingList(newList);
  };
  return (
    <CasamentoContext.Provider
      value={{ removeFromWeddingList, addToWeddingList, weddingList }}
    >
      {children}
    </CasamentoContext.Provider>
  );
};
