import Menu from "./components/menu";
import { CasamentoProvider } from "./providers/wedding";
import { ConfraternizacaoProvider } from "./providers/confraternization";
import { FormaturaProvider } from "./providers/graduation";
import Routes from "./routes";
import { DivApp } from "./styles/app";
function App() {
  return (
    <DivApp>
      <Menu></Menu>
      <FormaturaProvider>
        <CasamentoProvider>
          <ConfraternizacaoProvider>
            <Routes></Routes>
          </ConfraternizacaoProvider>
        </CasamentoProvider>
      </FormaturaProvider>
    </DivApp>
  );
}

export default App;
